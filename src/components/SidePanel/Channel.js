import React,{Component} from "react";
import {Menu,Icon, Modal,Form,Input, FormField, Button} from "semantic-ui-react";
import firebase from "../../firebase";
import {connect} from "react-redux";
import {setCurrentChannel} from "../../actions/index";

class Channel extends Component{
    state={
        channels:[],
        modal:false,
        channelName:"",
        channelDetails:"",
        channelRef:firebase.database().ref('channels'),
        user:this.props.currentuser,
        firstLoad:true,
        activeChannel:'' //this state will take the current id of the active channel
    }
    closeModal = () => {
        this.setState({
            modal:false
        })
    }
    openModal = () => {
        this.setState({
            modal:true
        })
    }
    handleChange = (event) =>{
        this.setState({
            [event.target.name]:[event.target.value]
        })
    }
    addChannel = () => {
        const {channelRef,channelName,channelDetails} = this.state;

        const key=channelRef.push().key;

        const newChannel = {
            id:key,
            name:channelName,
            details:channelDetails,
            createdBy:{
                // name:user.displayName,
                // avatar:user.photoURL
                name:'simpleuser',
                avatar:'new channel'

            }
        }

        channelRef
        .child(key)
        .update(newChannel)
        .then(()=>{
            this.setState({channelName:'',channelDetails:''});
            this.closeModal();
            console.log('the channel added')
        })
        .catch((err)=>{
            console.log(err);
        })
    }
    handleSubmit =  (event) => {
        event.preventDefault();
        if(this.isFormValid(this.state)){
            console.log('channel added');
            this.addChannel();
        }
    }
    changeChannel = channel => {
        console.log('the changeing channel is',channel);
     this.props.setCurrentChannel(channel);
     this.setactiveChannel(channel);
       
        
    }
    setactiveChannel = (channel)=>{
        this.setState({
            activeChannel:channel.id
        })
    }
    // displayChannels = channels =>{
        
    //     }
    isFormValid = ({channelName,channelDetails}) => channelName && channelDetails
    componentWillUnmount(){
        this.removeListeners();
    }
    removeListeners = () => {
        this.state.channelRef.off();
    }
    componentDidMount(){
        this.addListeners();
    }
    addListeners = () => {
        let loadedChannels = [];
        this.state.channelRef.on('child_added',snap =>{
            loadedChannels.push(snap.val());
            // console.log('the new channels',loadedChannels);
            this.setState({
                channels:loadedChannels
            },()=>this.setFirstChannel())
        })
    }
    
   setFirstChannel = () => {
       const  firstchannel =this.state.channels[0];
    if(this.state.firstLoad && this.state.channels.length > 0 ){
        this.props.setCurrentChannel(firstchannel);
        this.setactiveChannel(firstchannel);
    }
    this.setState({firstLoad:false})
   }
    render (){
    
    const {channels,modal,activeChannel} = this.state;

        return (
            <React.Fragment>
            <Menu.Menu inherit="true" style={{paddingBottom:"2em",background:"#4c3c4c"}}>
                <Menu.Item>
                <span >
                <Icon name="exchange"/>
                Channels
                </span>{" "}
                ({channels.length})<Icon name="add" inverted onClick={this.openModal}/>
                </Menu.Item>
                {/*channels display*/}
                {  channels.length > 0 && channels.map((channel)=> 
            
                <Menu.Item
                    key={channel.id}
            
                    onClick={()=>this.changeChannel(channel)}
                    name={channel.name[0]}
                    style={{opacity:'0.7em'}}
                    active ={channel.id == activeChannel}
                    >
                    #{channel.name[0]}
                    </Menu.Item> 
                )}
                <Menu.Item>
                <span >
                <Icon name="exchange"/>
                 Add new Channel
                </span>{" "}
                <Icon name="add" inverted onClick={this.openModal}/>
                </Menu.Item>
                {/*channels display*/}
                </Menu.Menu>

            {/**/}
            <Modal basic open={modal} onClose={this.onClose}>
            <Modal.Header>Add a Channel</Modal.Header>
            <Modal.Content>
            <Form onSubmit={this.handleSubmit}>
            <FormField>
            <Input 
            fluid
            label="Name of Channel"
            name="channelName"          //state 
            onChange={this.handleChange}
            />
            </FormField>
            <FormField>
            <Input 
            fluid
            label="About the Channel"
            name="channelDetails"
            onChange={this.handleChange}
            />
            </FormField>
            </Form>
            </Modal.Content>
            <Modal.Actions>
             <Button color="green" inverted onClick={this.handleSubmit}>
                <Icon name="checkmark"/>Add
             </Button>
             <Button color="red" inverted onClick={this.closeModal}>
                <Icon name="remove"/>Remove
             </Button>
            </Modal.Actions>
            </Modal>
    
            </React.Fragment>
            
        )
    }
}
export default connect(null,{setCurrentChannel})(Channel)