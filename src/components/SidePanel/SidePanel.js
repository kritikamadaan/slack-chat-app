import React ,{Component} from "react";
import {Menu} from "semantic-ui-react";
import UserPanel from "./UserPanel";
import Channel from "./Channel";

class SidePanel extends  Component{
state={
    channel:null,
    user:null
}
componentWillReceiveProps(nextProps){
        // console.log('the component will receive next props',nextProps.channel);
       this.setState({
           channel:nextProps.channel,
           user:nextProps.user
       })
    }
render(){
    console.log('the sidepanel state is',this.state);
    const {channel,user}=this.state;
    
    return(
     
      
        <Menu
        size="large"
        inverted
        fixed="left"
        vertical
        style={{backgound :"#4c3c4c",fontSize:"1.2rem"}}
        >
        <UserPanel currentuser={user}/>
        <Channel currentuser={channel}/>
        </Menu>
    
 
       
    )
    }
}
export default SidePanel


