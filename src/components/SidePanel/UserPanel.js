import React,{Component} from "react";
import {Grid,Header, Icon, Dropdown} from "semantic-ui-react";
import firebase from "../../firebase";
import {connect} from "react-redux";



class UserPanel  extends Component{


state={
     user:''
}

componentWillReceiveProps(nextProps){
    // console.log('the component will receive next props',nextProps.channel);
   this.setState({
       
       user:nextProps.currentuser
   })
}


    dropdownOptions = () => [
        {
            key:'user',
            text:<span>Signed in as <strong>{this.state.user.email}</strong></span>,
            disabled:false
        },
        {
            key:'avatar',
            text:<span>Change Avatar</span>
        },
        {
            key:'signout',
            text:<span onClick={this.handleSignout}>Sign out</span>
        }
    ]
    handleSignout = () => {
        firebase
        .auth()
        .signOut()      //signout function accepts no arguments arguments must be blank
        .then(() => {
            console.log('the user is sign out');
        })
    }
render()
    {
        const {user}=this.state;
        console.log('the state of the user in userpanel',this.state.user);
    return (
        <Grid style={{background:"#4c3c4c"}}>
        <Grid.Column>
        <Grid.Row style={{padding:'1.2em',margin:0}}>
        {/*App Header*/}
        <Header as='h2' inverted floated='left' >
        <Icon name="code"/>
        <Header.Content>DevChat..</Header.Content>
        <Header.Subheader inverted>
         {user.email}
      </Header.Subheader>
        </Header>
        </Grid.Row>

        {/*Header Dropdown*/}
        <Header style={{padding:"1.4em"}}as='h4'inverted>
        <Dropdown
     
        trigger={
            <span >User</span>
        }
        options={this.dropdownOptions()}
       
       
        />
        </Header>
        </Grid.Column>
        </Grid>
    )
}
}
const mapStateToProps = state =>(
    
    {
    
    currentuser:state.user.currentUser
   
})
export default connect(mapStateToProps,null)(UserPanel)