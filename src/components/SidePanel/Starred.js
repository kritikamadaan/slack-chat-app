import React, { Component } from "react";
import {Menu,Icon} from "semantic-ui-react";

class Starred extends  Component{
    render(){
        return(
            <Menu.Menu inherit="true" style={{paddingBottom:"2em",background:"#4c3c4c"}}>
        <Menu.Item>
        <span >
        <Icon name="exchange"/>
        Channels
        </span>{" "}
        ({starredChannels.length})<Icon name="add" inverted onClick={this.openModal}/>
        </Menu.Item>
        {this.displayChannels(starredChannels)}
            </Menu.Menu>    
        )
    }
}
export default Starred;