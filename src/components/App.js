import React from 'react';
import './App.css';
import {Grid} from "semantic-ui-react";

import ColorPanel from "./ColorPanel/ColorPanel";
import SidePanel from "./SidePanel/SidePanel";
import Messages from "./Messages/Messages";
import MetaPanel from "./MetaPanel/MetaPanel";
import {connect} from "react-redux";

const App = ({currentuser,currentchannel}) =>{
  console.log('the current channel is ',currentchannel)
 return( <Grid columns="equal" className="app" style={{backgroundColor:"#eee"}}>
 
{/*ColorPanel*/}
  
  <Grid>
  <SidePanel
  channel={currentchannel}
  user={currentuser}/>
  </Grid>
  
  <Grid.Column  width={6} style={{marginLeft:"320px"}}>
  <Messages
  // key={currentchannel && currentchannel.id}
  user={currentuser}
  channel={currentchannel}/>
  </Grid.Column>
  <Grid.Column width={4}>
  <MetaPanel/>
  </Grid.Column>
  
  </Grid>
 )
}
const mapStateToProps = (state) =>

(
 {
  // console.log('the app channel state is',state.channel.payload) ,
  // console.log('the app user state is',state.user.currentUser)
  currentuser:state.user.currentUser,
   currentchannel:state.channel.payload
})
export default connect(mapStateToProps)(App)