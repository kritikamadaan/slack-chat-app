import React from "react";
import {Grid,Form,Segment,Button,Header,Message,Icon} from 'semantic-ui-react';
import {Link} from "react-router-dom";
import firebase  from "../../firebase";
import md5 from 'md5';



class Register extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            username:'',
            email:  '',
            password:'',
            confirmpassword:'',
            errors:[],
            loading:false,
            userRef:firebase.database().ref('users')
        }
    }
    isFormValid = () =>{
        let errors = []
        let error;
        if(this.isFormEmpty(this.state)){
            error ={message :  'Fill in all the fields'};
            this.setState({
                errors:errors.concat(error)
            })
            return false
        }
        else if(this.isPasswordValid(this.state)){
            error = {message :'Password is wrong'};
            console.log('the error is ',error);
            this.setState({
                errors :errors.concat(error)
                
            })
            return false
        }
        else{
            return true
          
        }
    }
    isFormEmpty = ({username,email,password,confirmpassword}) => {
        return !username.length || !email.length || !password.length || !confirmpassword.length 

    }
    isPasswordValid = ({password,confirmpassword}) => {
        if(password.length < 6 || confirmpassword.length < 6){
            return false
        }
        else if (password !== confirmpassword){
            return false
        }  
        else {
            return true
        }
    }
     errorDisplay =  errors => {
        errors.map((error,i)=>{
            console.log('the error is ',error.message);
            return <p key={i}>{error.message}</p>
        })
     }
    handleChange = (event) =>{
        this.setState({
            [event.target.name]:[event.target.value]
        })
    }
    handleSubmit = event =>{
        event.preventDefault();
        console.log('the email is ',this.state.email);
        
        console.log('the password consist',this.state.password)
        if(this.isFormValid())
       {
           this.setState({
               errors:[],
               loading:true
           })
        firebase
        .auth()
        .createUserWithEmailAndPassword(this.state.email.toString(),this.state.password.toString())
        .then(createuser =>{
            // var user=firebase.auth().currentuser;
            console.log('the created user is',createuser);
            
            createuser.user.updateProfile({
                    displayName:this.state.username,
                    // photoURL:`https://gravatar.com/avatar/${md5(createuser.user.email)}?d=identicon`
                })
                .then(()=>
                this.setState({
                    loading:false
    
                }))
                .catch((err)=>{
                    console.log(err)
                    this.setState({errors:this.state.errors.concat(err),loading:false})
                })
           this.saveUser(createuser)
                .then(()=>{
                    console.log('user is logged')
                })
            .catch(err =>{
                console.log(err)
            })
            this.setState({
                loading:false
            })
        })
        .catch(err =>{
            console.error(err);
            this.setState({
                loading:false,
                errors:this.state.errors.concat.err
            })
        })
       }
       else {
           return false;
       }
    }
    handleInputError = (errors,inputname) => {
      return   errors.some(error => error.message.toLowerCase().includes(inputname))
        ? "error"
        : ""
        

    }
    saveUser = (createuser) => {
        console.log('the uid ',createuser.user.uid);
        return this.state.userRef.child(createuser.user.uid).set({
            name:createuser.user.displayName
        })
    }

    render(){
        const {username,email,password,confirmpassword,loading,errors}=this.state;
       
        return(
            
            <Grid divided textAlign="center" verticalAlign="middle" className="app">
            <Grid.Column style={{maxWidth: 450}}>
                <Header as='h2' color='orange' icon textAlign='center'>
                    <Icon name="puzzle piece" color='orange'/>
                    Register for  
                </Header>
                <Form  size="large" onSubmit={this.handleSubmit}>
                    <Segment stacked>
                        <Form.Input fluid  label='Username' placeholder='username' icon="user" iconPosition='left' type='text' name="username" value={username} className={this.handleInputError(errors,'email')} />
                        <Form.Input fluid  label='Email' placeholder='email address' icon="mail" iconPosition='left' type='email'  name="email" value={email} className={this.handleInputError(errors,'email')}  />
                        <Form.Input fluid label='Password' placeholder='Enter password' icon='lock' iconPosition='left' type='password'   name="password" value={password} className={this.handleInputError(errors,'password')} />
                        <Form.Input fluid  label='Confirm Password' placeholder='Password Confirmation' icon="repeat" iconPosition='left' name="confirmpassword" value={confirmpassword} type='password' className={this.handleInputError(errors,'password')} />
                        <Button fluid  disabled={loading} className={loading ?  'loading' :''} color='orange' size='large'>Submit</Button>
                    </Segment>
                </Form>
                {/* {errors.length > 0 &&(
                //     <Message error>
                //     <h3>Errors</h3>
                //     {this.errorDisplay(errors)}
                //     </Message>
                // )
            }*/}
                <Message>Already a user ? <Link to='/login'>Login</Link> </Message>
                
            </Grid.Column>  
           
            </Grid>
            
            
            
        )
        
    }
    
 
}
export default Register




// createuser.user.updateProfile({
//     displayName: this.state.username,
//     photoURL:`http://gravatar.com/avatar/${md5(createuser.user)}?d=identicon`
// })
// .then(()=>
// this.setState({loading:false})
// )
// .catch(err =>{
//     console.log(err);
//     this.setState({errors:this.state.errors.concat(err),loading:false})
// })