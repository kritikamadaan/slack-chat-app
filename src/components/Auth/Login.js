import React from "react";
import {Grid,Form,Segment,Button,Header,Message,Icon} from 'semantic-ui-react';
import {Link} from "react-router-dom";
import firebase  from "../../firebase";



class Login extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            username:"",
            email:  "",
            password:"",
            confirmpassword:"",
            errors:[],
            loading:false,
           
        }
    }
  
    
    
   
    handleChange = (event) =>{
        this.setState({
            [event.target.name]:[event.target.value]
        })
    }
    handleSubmit = event =>{
        event.preventDefault();
        if(this.isFormValid(this.state)){
            this.setState({errors:[],loading:true})
           firebase
           .auth()
           . signInWithEmailAndPassword('kritika123112@gmail.com','passwor11d1223')
           .then(signinuser =>{
               console.log('the sign in user',signinuser)
           })
           .catch(err=>{
               console.log(err);
               this.setState({loading:false,errors:this.state.errors.concat(err)})
           })
        }
        
    }
    isFormValid = ({email,password}) => email && password

    handleInputError = (errors,inputname) => {
      return   errors.some(error => error.message.toLowerCase().includes(inputname))
        ? "error"
        : ""
    }
    errorDisplay =  errors => {
        errors.map((error,i)=>{
            console.log('the error is ',error.message);
            return <p key={i}>{error.message}</p>
        })
     }

    render(){
        const {email,password,loading,errors}=this.state;
       
        return(
            
            <Grid divided textAlign="center" verticalAlign="middle" className="app">
            <Grid.Column style={{maxWidth: 450}}>
                <Header as='h2' color='violet' icon textAlign='center'>
                    <Icon name="code branch" color='violet'/>
                    Login to Devchat
                </Header>
                <Form  size="large" onSubmit={this.handleSubmit}>
                    <Segment stacked>
                        
                        <Form.Input fluid  label='Email' placeholder='email address' icon="mail" iconPosition='left' type='email'  name="email" value={email} onChange={this.handleChange} />
                        <Form.Input fluid label='Password' placeholder='Enter password' icon='lock' iconPosition='left' type='password'   name="password" value={password} onChange={this.handleChange} />
                       
                        <Button fluid  disabled={loading} className={loading ?  'loading' :''} color='violet' size='large'>Submit</Button>
                    </Segment>
                </Form>
                 {this.state.errors.length > 0 &&(
                    <Message error>
                    <h3>Errors</h3>
                    {this.errorDisplay(this.state.errors)}
                    </Message>
                )
            }
                <Message>Don't have an account? <Link to='/register'>Register</Link> </Message>
                
            </Grid.Column>  
           
            </Grid>
            
            
            
        )
        
    }
    
 
}
export default Login



