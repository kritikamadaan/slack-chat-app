import React ,{Component, Fragment} from "react";
import {Sidebar,Menu,Divider,Button,Modal,Label,Icon} from "semantic-ui-react";

import { SliderPicker } from 'react-color';

class ColorPanel extends Component{
    state={
        modal:false
    }
    openModal = () => {
        this.setState({
            modal:true
        })
    }
    closeModal = () => {
        this.setState({
            modal:false
        })
    }
render(){
    const {modal}=this.state;
    return(
        <Fragment>
        <Sidebar
        as={Menu}
        animation='overlay'
        inverted
        vertical
        icon="labeled"
        visible="false"
        
        >
        <Divider/>
        <Button icon="add" color="blue" size="small"/>
        {/* color Picker modal */}
        <Modal basic open={modal} onClose={this.closeModal}>
        <Modal.Header>Choose app color</Modal.Header>
        <Modal.Content>
            <Label content="Primary Color"/>
            <SliderPicker/>
            <Label content="Secondary color"/>
            <SliderPicker/>
        </Modal.Content>
        <Modal.Actions>
        <Button color="green"inverted>
        <Icon name="checkmark"/>save
        </Button>
        <Button color="red"inverted>
        <Icon name="remove"/>cancel
        </Button>
        </Modal.Actions>
        </Modal>
        </Sidebar>
        </Fragment>
    )
    }
}
export default ColorPanel