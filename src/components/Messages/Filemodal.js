import React,{Component} from "react";
import {Modal, Input,Button,Icon} from "semantic-ui-react";

class FileModal extends Component{
    state={
        file:null
    }
    addFile = event =>  {
        console.log('the event is',event);
        const file = event.target.files[0]
        console.log('the input file is',file);
        if(file){
            this.setState({file:file})
        }
    }
    sendFile = event => {
        
    }
    render(){
        const {modal,closeModal}=this.props;
        return(
            <Modal  basic open={modal} onClose={closeModal}>
            <Modal.Header>Select a Image File</Modal.Header>
            <Modal.Content>
            <Input
            fluid
            label="Filetype:.jpg,.png"
            name="flie"
            type="file"
            onClick={this.addFile}
            />
            </Modal.Content>
            <Modal.Actions>
            <Button color="green" inverted onClick={this.sendFile}>
            <Icon name="checkmark"/>Send
            </Button>
            <Button color="red" inverted  onClick={closeModal}>
            <Icon name="remove"/>cancel
            </Button>
            </Modal.Actions>
            </Modal>
        )
    }
}
export default FileModal