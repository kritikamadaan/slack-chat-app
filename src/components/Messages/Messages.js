import React ,{Component} from "react";
import {Segment,Comment, Message} from "semantic-ui-react";
import MessagesForm from "./MessagesForm";
import MessagesHeader from "./MessagesHeader";
import firebase from "../../firebase";


class Messages extends Component{
    state={
        messagesRef:firebase.database().ref('messages'),
        channel:null,
        user:null,
        messages:[],
        messagesLoading:true
    }
    componentWillReceiveProps(nextProps) 
    { 
       
          console.log('the component will receive next props',nextProps.channel);
          console.log('the component will userrecieve props ',nextProps.user);
       this.setState({
           channel:nextProps.channel,
           user:nextProps.user
       })
       
    
    }
    // static getDerivedStateFromProps(props, state) {
    //     console.log('the component will receive next props',props.channel);
    //       console.log('the component will userrecieve props ',props.user);
       

            
          
    //    this.setState({
    //        channel:props.channel,
    //        user:props.user
    //     });
    
  
    // }

    componentDidMount = () => {
        const {channel,user}=this.state;
       
        console.log('the props',this.props);
        console.log('the state',this.state);
        if(channel && user){
            console.log('channel.id: ', channel.id);
            this.addListeners(channel.id)
        }
      
    }
    addListeners = channelId => {
        const {channel}=this.state;
        console.log('the channel id',channelId)
        this.addMessageListener(channelId)
    }
    addMessageListener =  channelId => {
        
        const {channel,user}=this.state;
         
        let loadedMessages = [] ;
        this.state.messagesRef.child(channelId).on('child_added',snap => {
            loadedMessages.push(snap.val());
            console.log('the loaded messages',loadedMessages)
            this.setState({
                messages:loadedMessages,
                messagesLoading:false
            })
        })
    }
    displayMessages = messages => {
        messages.length > 0 && messages.map(message =>(
            <Message
            key={message.timestamp}
            message={message}
            user={this.state.user}
            />
        ))
    }
render(){
   
    const {messagesRef,messages,channel,user}=this.state;
   
    return(
       <React.Fragment>
       <MessagesHeader messagesRef={messagesRef}/>
       <Segment>
       <Comment.Group className="messages">
       {this.displayMessages(messages)}
       </Comment.Group>
       </Segment>
       <MessagesForm
       currentchannel={channel}
       currentuser={user}
        messagesRef={messagesRef}/>
       </React.Fragment>
    )
    }
}
export default Messages