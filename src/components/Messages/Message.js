import React from "react";
import {Comment} from "semantic-ui-react";


const Message = ({message,user}) => {
    return(
       <Comment>
       <Comment.Avatar src={message.user.Avatar}/>
       <Comment.Content className={isOwnMessage(message,user)}>
       </Comment.Content>
       </Comment> 
    )
}
export default Message