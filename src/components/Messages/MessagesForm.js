import React from "react";
import {Segment, Input, Button} from "semantic-ui-react";
import firebase from "../../firebase";
import FileModal from "./Filemodal";

class MessagesForm extends React.Component{
    state = {
        message:'',
        loading:false,
        channel:"",
        user:"",
        errors:[],
        modal:false
    }
    openModal = () => {this.setState({modal:true})}
    closeModal = () => {this.setState({modal:false})}

    componentWillReceiveProps(nextProps){
        this.setState({
            channel:nextProps.currentchannel,
            user:nextProps.currentuser
        })
    }
    
    handleChange =  event => {
       this.setState({
        [event.target.name]:[event.target.value]
       })
    }
    createMessage = () => {
        console.log('the user is',this.state.user);
        const message = {
            // timestamp:firebase.database.ServerValue.TIMESTAMP,
            user:{
                id:this.state.user.uid,
                name:this.state.user.displayName,
                avatar:this.state.user.photoURL
            },
            content:this.state.message
        }
        return message;
    }
    sendMessages = () =>{
        console.log('the send messgae',this.props,this.state)
        const {messagesRef}=this.props;
        const {message,channel}=this.state;
       
        if(message){
            this.setState({loading:true})
            messagesRef
            .child(channel.id)
            .push()
            .set(this.createMessage())
            .then(()=>{
                this.setState({loading:false,message:'',errors:[]})
            })
            .catch((err)=>{
                console.log(err);
                this.setState({loading:false,errors:this.state.errors.concat(err)})
            })
        }
        else{
            this.setState({
                errors:this.state.errors.concat({message:"Add a message"})
            })
        }
    }
    render(){
        const {errors,loading,message,modal}=this.state;
        return(
            <Segment className="messages_form">
                <Input
                fluid
                onChange={this.handleChange}
                name="message"
                style={{marginBottom:'0.7em'}}
                label={<Button icon={'add'}/>}
                labelPosition="left"
                placeholder="Write Your Message"
                value={message} 
                className={
                    errors.some(error=> error.message.includes('message')) ? 'error' :null
                }
                />
                <Button.Group icon width="4">
                <Button
                onClick={this.sendMessages}
                color="orange"
                content="Add Reply"
                labelPosition="left"
                icon="edit"
                disabled={loading}
                />
                <Button
                color="teal"
                content="Upload Media"
                labelPosition="right"
                icon="cloud upload"
                onClick={this.openModal}
                />
                <FileModal
                modal={modal}
                closeModal={this.closeModal}
                />
                </Button.Group>
            </Segment>
        )
    }
}
export default MessagesForm