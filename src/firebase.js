import firebase from 'firebase';

import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';

var config = {
    apiKey: "AIzaSyD0hNaSUV10BHvP2AvpWOEuARYxjHt0Sro",
    authDomain: "slack-app-757cf.firebaseapp.com",
    databaseURL: "https://slack-app-757cf.firebaseio.com",
    projectId: "slack-app-757cf",
    storageBucket: "slack-app-757cf.appspot.com",
    messagingSenderId: "292960275121",
    appId: "1:292960275121:web:713b9bd11732147322f090",
    measurementId: "G-NP0RVF50TN"
  };
firebase.initializeApp(config);

export default firebase