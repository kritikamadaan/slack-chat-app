import React from "react";
import {Loader,Dimmer} from "semantic-ui-react";

const Spinner = () => {
    return (
      <Dimmer active  style={{marginTop:"720px",height:"20vh"}}>
        <Loader content={'Preparing chat......'}/>
      </Dimmer> 
      )
}
export default Spinner