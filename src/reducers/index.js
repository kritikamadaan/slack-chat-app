import * as actiontypes from "../actions/types";
import {combineReducers} from "redux";
//userReducer
const initialuserstate ={
    currentUser:null,
    isloading:false
}
export const userReducer = (state = initialuserstate ,action) => {
    switch(action.type){
        case actiontypes.SET_USER :
            console.log('the reducer current user',action.payload.createuser);
            return{
                currentUser:action.payload.createuser,
                isloading:true
            }
        case actiontypes.CLEAR_USER :
            return {
                ...state,
                loading:false
            }
        default: return state
    }
}

//channel_reducer
const initialChannelState ={
    currentChannel:null
}
const channelreducer = (state = initialChannelState,action) => {
    switch(action.type){
        case actiontypes.SET_CURRENT_CHANNEL :
            console.log('the reducer current channel',action.payload.currentChannel);
            return{
                ...state,
                payload:action.payload.currentChannel
            }
        default:
            return state;
    }
}
 const rootReducer =combineReducers({
    
        user:userReducer,
     channel :channelreducer 
})
export default rootReducer;
