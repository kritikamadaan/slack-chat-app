import * as actiontypes from "./types";

//user actions
export const setuser = user => {
 console.log('the set user  action is',user);
    return {
        type:actiontypes.SET_USER,
        payload:{
            createuser : user
        }
    }
}
export const clearuser =  () =>{
    return {
        type:actiontypes.CLEAR_USER
    }
}
//channel actions
export const setCurrentChannel = channel => {
    console.log('the action  heychannel is',channel)
    return{
        type:actiontypes.SET_CURRENT_CHANNEL,
        payload:{
            currentChannel:channel
        }
        
    }
}