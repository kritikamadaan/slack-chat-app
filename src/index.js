import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

import {BrowserRouter as Router,Switch,Route,withRouter} from "react-router-dom";
import Login from "./components/Auth/Login";
import Register from "./components/Auth/Register";

import 'semantic-ui-css/semantic.min.css';

import firebase  from "./firebase";

//
import {createStore} from "redux";
import {Provider} from "react-redux";
import {connect} from "react-redux";
import {composeWithDevTools} from "redux-devtools-extension";
import rootReducer from "./reducers/index"
import {setuser,clearuser} from "./actions/index";

//import Spinner from "./Spinner";



const store=createStore(rootReducer,composeWithDevTools());

class Root extends React.Component{
    componentDidMount(){
        firebase.auth().onAuthStateChanged(user =>{
            console.log('the auth state changed user is ',this.props.setuser)
            console.log('the map state to is loading',this.props.isloading);
            console.log('the clear user',this.props.clearuser);
            if(user){
                this.props.setuser(user);
                // this.props.history.push("/");
            }
            else{
            // this.props.history.push('/login');
                this.props.clearuser();
            }
        });
    }
    render(){
        //  return this.props.isloading ? <Spinner/> :(
            // <Router>

           return(
               <React.Fragment>
              
               <Switch>
            <Route exact path="/"component ={App}/>
            <Route path ="/login" component={Login}/>
            <Route path="/register" component={Register}/>
            </Switch>
            </React.Fragment> 
            // </Router>

        )
    }

    
}
const mapStateToProps = state => ({
    isloading :state.user.isloading
})
   


const RouterwithAuth = withRouter(connect(mapStateToProps,{setuser,clearuser})(Root));
ReactDOM.render(<Provider store={store}><Router><RouterwithAuth/></Router></Provider>, document.getElementById('root'));



